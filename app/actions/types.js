export const FETCH_VIDEOS = 'fetch_videos'
export const SET_SEARCH = 'set_search'
export const PLAY_VIDEO = 'play_video'
export const CLOSE_VIDEO = 'close_video'
