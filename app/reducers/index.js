import { combineReducers } from 'redux';
import videos from './videos_reducer';

const rootReducer = combineReducers({
  videos
})

export default rootReducer