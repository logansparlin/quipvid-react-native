import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';

import { Router, Scene } from 'react-native-router-flux';
import { connect, Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import reducers from './reducers'

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);
const store = createStoreWithMiddleware(reducers)

import App from './components/app'


class quipvid extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router hideNavBar={true}>
            <Scene key="home" initial={true} name="home" type="push" component={App} title="App" />
        </Router>
      </Provider>
    );
  }
}

export default quipvid;
