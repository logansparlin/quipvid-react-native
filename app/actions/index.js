import {
  FETCH_VIDEOS,
  SET_SEARCH,
  PLAY_VIDEO,
  CLOSE_VIDEO
} from './types'
import axios from 'axios'

export function fetchVideos() {
  return function(dispatch) {
    axios.get('https://quipvid.com/api/quips')
    .then(res => {
      dispatch({
        type: FETCH_VIDEOS,
        payload: res.data
      })
    })
  }
}

export function setSearch(search) {
  return {
    type: SET_SEARCH,
    payload: search
  }
}

export function playVideo(video) {
  return {
    type: PLAY_VIDEO,
    payload: video
  }
}

export function closeVideo() {
  return {
    type: CLOSE_VIDEO
  }
}
