import React, { Component } from 'react'
import SearchBar from 'react-native-search-bar'
import {
  View,
  Text,
  StyleSheet,
  ListView,
  Dimensions,
  TouchableOpacity,
  TouchableWithoutFeedback,
  StatusBar,
  Image
} from 'react-native'

import { connect } from 'react-redux'
import * as actions from '../actions'

import VideoRow from './video_row'
import VideoPlayer from './video_player'

let { width, height } = Dimensions.get('window')

class App extends Component {

  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
    this.renderVideoPlayer = this.renderVideoPlayer.bind(this)
    this.renderRow = this.renderRow.bind(this)
    this.searchVideos = this.searchVideos.bind(this)
    this.state = {
      dataSource: ds.cloneWithRows(this.props.videos)
    };
  }

  componentWillMount() {
    this.props.fetchVideos()
  }

  searchVideos(videos, searchText) {
    let filteredVideos = videos.filter(video => {
      return video.title.toLowerCase().indexOf(searchText.toLowerCase()) >= 0
    })

    return filteredVideos
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(this.searchVideos(nextProps.videos, nextProps.search))
    })
  }

  renderRow(video) {
    return <VideoRow video={video} playVideo={this.props.playVideo} />
  }

  renderVideoPlayer() {
    if(this.props.activeVideo) {
      return <VideoPlayer video={this.props.activeVideo} closeVideo={this.props.closeVideo} />
    }
  }

  render() {
    if(this.props.videos.length < 1) {
      return (
        <View style={styles.container}>
          <Text style={styles.welcome}>
            Loading...
          </Text>
        </View>
      )
    }
    return (
      <View style={{flex: 1}}>
        <View style={styles.container}>
          <StatusBar barStyle="light-content" />
          <Text style={styles.welcome}>
            QuipVid
          </Text>
          <SearchBar
            onChangeText={(text) => {
              this.refs.listview.scrollTo({y: 0})
              this.props.setSearch(text)
            }}
            placeholder="Search"
            barStyle="black"
            height={40}
          />
          <ListView
            ref="listview"
            contentContainerStyle={styles.listView}
            dataSource={this.state.dataSource}
            renderRow={this.renderRow}
            enableEmptySections={true}
            initialListSize={10}
            keyboardShouldPersistTaps={false}
            keyboardDismissMode="on-drag"
            />
        </View>
        {this.renderVideoPlayer()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width: width,
    height: height,
    backgroundColor: '#eee',
  },
  welcome: {
    fontSize: 16,
    textAlign: 'center',
    padding: 10,
    paddingTop: 25,
    fontWeight: 'bold',
    letterSpacing: 0.5,
    backgroundColor: 'rgb(30, 30, 30)',
    color: 'white'
  },
  listView: {
    backgroundColor: '#eee',
    marginTop: 10,
    marginBottom: 5
  }
});

function mapStateToProps(state) {
  return {
    activeVideo: state.videos.activeVideo,
    search: state.videos.search,
    videos: state.videos.items
  }
}

export default connect(mapStateToProps, actions)(App);
