import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity
} from 'react-native'

export default class CloseIcon extends Component {

  static propTypes = {
    onPress: React.PropTypes.func,
    styles: React.PropTypes.object
  }

  render() {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={this.props.onPress}
        style={[styles.container, this.props.style  ]}>
        <View style={[styles.bar, styles.barOne]} />
        <View style={[styles.bar, styles.barTwo]} />
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    width: 30,
    height: 30,
    top: 35,
    right: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bar: {
    position: 'absolute',
    width: 30,
    height: 2,
    backgroundColor: 'white'
  },
  barOne: {
    transform: [
      {
        "rotate": "45deg"
      }
    ]
  },
  barTwo: {
    transform: [
      {
        "rotate": "-45deg"
      }
    ]
  }
})
