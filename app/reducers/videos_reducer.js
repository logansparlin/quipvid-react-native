import {
  FETCH_VIDEOS,
  SET_SEARCH,
  PLAY_VIDEO,
  CLOSE_VIDEO
} from '../actions/types';

const initialState = {
  search: '',
  activeVideo: null,
  items: []
}

export default function(state = initialState, action) {
  switch(action.type) {
    case FETCH_VIDEOS:
      return {
        ...state,
        items: action.payload
      }
    case SET_SEARCH:
      return {
        ...state,
        search: action.payload
      }
    case PLAY_VIDEO:
      return {
        ...state,
        activeVideo: action.payload
      }
    case CLOSE_VIDEO:
      return {
        ...state,
        activeVideo: null
      }
  }

  return state
}
