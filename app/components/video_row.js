'use strict';
import React, { Component } from 'react';
// import ActivityView from 'react-native-activity-view'
import {
  TouchableWithoutFeedback,
  TouchableOpacity,
  ActionSheetIOS,
  StyleSheet,
  Dimensions,
  Image,
  View,
  Text
} from 'react-native'

let {width, height} = Dimensions.get('window')

class VideoRow extends Component {

  static propTypes = {
    video: React.PropTypes.object.isRequired
  }

  static defaultProps = {
    video: {}
  }

  shareVideo() {
    ActionSheetIOS.showShareActionSheetWithOptions({
      message: 'The share message',
      subject: 'The share subject'
    },
    (error) => console.log(error),
    (success, method) => {
      if(success) {
        console.log('you shared it, congrats!')
      } else {
        console.log('better luck next time')
      }
    });
  }

  render() {
    let { video } = this.props
    return (
      <TouchableOpacity
        ref="share"
        style={styles.container}
        activeOpacity={0.8} onPress={() => this.props.playVideo(video)}>
        <Image style={styles.videoImage} source={{uri: video.image}} />
        <Text style={styles.videoTitle}>{video.title.toUpperCase()}</Text>
        <TouchableOpacity onPress={this.shareVideo} activeOpacity={0.7}>
          <Text>SHARE</Text>
        </TouchableOpacity>
      </TouchableOpacity>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    margin: 20,
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: 'white',
    shadowColor: 'rgb(50, 50, 50)',
    shadowOffset: {width: 1, height: 1},
    shadowOpacity: 0.5,
    shadowRadius: 2
  },
  videoImage: {
    height: 175,
  },
  videoTitle: {
    backgroundColor: 'rgba(0, 0, 0, 0)',
    color: '#333',
    letterSpacing: 1.5,
    fontWeight: 'bold',
    padding: 10
  },
})

export default VideoRow;
