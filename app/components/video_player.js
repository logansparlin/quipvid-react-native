import React, { Component } from 'react'
import Video from 'react-native-video'
import Slider from 'react-native-slider'
import {
  View,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  PanResponder,
  Dimensions,
  StyleSheet,
  Animated,
  Image
} from 'react-native'

let {width, height} = Dimensions.get('window')
import CloseIcon from './common/close_icon'

export default class VideoPlayer extends Component {

  static propTypes = {
    video: React.PropTypes.object.isRequired,
    closeVideo: React.PropTypes.func.isRequired
  }

  constructor(props) {
    super(props);
    this._onLoad = this._onLoad.bind(this)
    this._playVideo = this._playVideo.bind(this)
    this._pauseVideo = this._pauseVideo.bind(this)
    this._closeVideo = this._closeVideo.bind(this)
    this._onProgress = this._onProgress.bind(this)
    this._isDonePlaying = this._isDonePlaying.bind(this)
    this._trackToPosition = this._trackToPosition.bind(this)
    this.renderActionIcon = this.renderActionIcon.bind(this)
    this.state = {
      pan: new Animated.ValueXY(),
      loaded: false,
      currentTime: 0,
      duration: 0,
      playing: true,
      controls: true
    };
  }

  componentWillMount() {
    this._panResponder = PanResponder.create({
      onMoveShouldSetResponderCapture: () => true,
      onMoveShouldSetPanResponderCapture: () => true,
      onMoveShouldSetPanResponder: () => true,
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: Animated.event([
        null, {dx: this.state.pan.x, dy: this.state.pan.y},
      ]),
      onPanResponderGrant: this._handlePanResponderGrant.bind(this),
      onPanResponderRelease: this._handlePanResponderEnd.bind(this),
      onPanResponderTerminate: this._handlePanResponderEnd.bind(this),
    })
  }

  _handlePanResponderEnd(e: Object, gestureState: Object) {
    let offset = this.state.pan.y._value
    if(offset >= 100) {
      this._closeVideo('down')
      return
    } else if(offset <= -100) {
      this._closeVideo('up')
      return
    }

    this.setState({ playing: true })

    Animated.spring(
      this.state.pan,
      {
        toValue: {x: 0, y: 0 },
        friction: 5
      }
    ).start()
  }

  _handlePanResponderGrant(e: Object, gestureState: Object) {
    this.setState({
      controls: !this.state.controls
    })
  }

  _closeVideo(direction) {
    let {closeVideo} = this.props;
    this.setState({ controls: false })
    Animated.timing(
      this.state.pan,
      {
        toValue: {x: 0, y: (direction == 'down') ? 800 : -800 },
        duration: 300
      }
    ).start(function() {
      closeVideo()
    });
  }

  _onLoad(data) {
    this.setState({
      loaded: true,
      duration: data.duration
    })
  }

  _onProgress(data) {
    this.setState({
      currentTime: data.currentTime
    })
  }

  _getTimePercentage(time, duration) {
    if(time > 0) {
      return parseFloat((time / duration))
    }

    return 0
  }

  _trackToPosition(data) {
    this.setState({
      playing: false,
      currentTime: this.state.duration * data
    })

    this.refs.videoPlayer.seek(this.state.duration * data)
  }

  _slidingComplete() {
    this.setState({ playing: true })
  }

  _isDonePlaying() {
    let {currentTime, duration} = this.state;
    return currentTime >= (duration - 0.25)
  }

  _pauseVideo() {
    this.setState({
      playing: false
    })
  }

  _playVideo() {
    if(this._isDonePlaying()) {
      this.refs.videoPlayer.seek(0)
    }

    this.setState({
      playing: true
    })
  }

  renderActionIcon() {
    if(this.state.playing && this.state.loaded && !this._isDonePlaying()) {
      return (
        <TouchableOpacity
          style={styles.action}
          activeOpacity={0.8}
          onPress={this._pauseVideo}>
          <Image
            source={require('../img/pause-icon.png')}
            style={styles.actionIcon}/>
        </TouchableOpacity>
      )
    } else {
      return (
        <TouchableOpacity
          style={styles.action}
          activeOpacity={0.8}
          onPress={this._playVideo}>
          <Image
            source={require('../img/play-icon.png')}
            style={styles.actionIcon}/>
        </TouchableOpacity>
      )
    }
  }

  _toggleControls() {
    console.log('controls')
  }

  render() {
    let { pan, currentTime, duration } = this.state

    let timePercent = this._getTimePercentage(currentTime, duration)

    let [translateX, translateY] = [pan.x, pan.y];
    let videoStyle = {transform: [{translateY}]};

    let interpolatedOpacity = pan.y.interpolate({
      inputRange: [-400, -100, 0, 100, 400],
      outputRange: [0, 1, 1, 1, 0],
      extrapolate: 'clamp'
    })

    return (
      <TouchableWithoutFeedback onPress={() => console.log('TOUCHED')}>
        <View style={styles.videoPlayerContainer}>
          <Animated.View style={[styles.overlay, {opacity: interpolatedOpacity}]} />
          <Animated.View
            style={[styles.videoPlayer, videoStyle]}
            {...this._panResponder.panHandlers}>
            <Video
              ref="videoPlayer"
              style={styles.videoPlayer}
              source={{uri: this.props.video.video}}
              rate={1}
              paused={(this.state.playing) ? false : true}
              onLoad={this._onLoad}
              volume={1}
              repeat={false}
              onProgress={this._onProgress}
              />
          </Animated.View>
          <Animated.View style={[styles.bottomControls, (this.state.controls) ? {} : styles.controlsHidden]}>
            {this.renderActionIcon()}
            <Slider
              style={styles.slider}
              value={timePercent}
              onValueChange={this._trackToPosition.bind(this)}
              maximumTrackTintColor="white"
              minimumTrackTintColor="green"
              thumbTintColor="green"
              trackStyle={{height: 2}}
              thumbStyle={{width: 10, height: 10}}
              onSlidingComplete={this._slidingComplete.bind(this)}
            />
          </Animated.View>
          <Animated.View style={[styles.topControls, (this.state.controls) ? {} : styles.controlsHidden]}>
            <CloseIcon onPress={this.props.closeVideo} style={styles.icon}/>
          </Animated.View>
        </View>
      </TouchableWithoutFeedback>
    )
  }

}

const styles = StyleSheet.create({
  videoPlayerContainer: {
    position: 'absolute',
    top: 0, left: 0, right: 0, bottom: 0,
    backgroundColor: 'transparent',
    flexDirection: 'column'
  },
  videoPlayer: {
    flex: 1
  },
  placeholder: {
    position: 'absolute',
    top: 0, left: 0, right: 0, bottom: 0,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  overlay: {
    position: 'absolute',
    top: 0, left: 0, right: 0, bottom: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.9)'
  },
  bottomControls: {
    position: 'absolute',
    bottom: 0, left: 0, right: 0,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 10,
    paddingTop: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  topControls: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0
  },
  controlsHidden: {
    opacity: 0
  },
  slider: {
    flex: 1,
    marginRight: 10
  },
  action: {
    padding: 10,
  },
  actionIcon: {
    width: 20,
    height: 20
  }
})
